let winProbability = 0;

$('#pee').on('click', pee);
$('#poop').on('click', poop);
$('#toilet').on('click', getPrize);
$('#flow1').on('click', {probability: 0}, setFlow);
$('#flow2').on('click', {probability: 0.1}, setFlow);
$('#flow3').on('click', {probability: 0.2}, setFlow);

function pee() {
    $('#poop').hide();
    $('#pee').hide();
    $('.btn-flow-1').show();
    $('.btn-flow-2').show();
    $('.btn-flow-3').show();
}
function poop() {
    $('#poop').hide();
    $('#pee').hide();
    $('#toilet').show();
    winProbability = 1;
}

function setFlow(event) {
    $('#flow1, #flow2, #flow3').hide();
    $('#toilet').show();
    winProbability = event.data.probability;
    console.log(winProbability);
}

var probability = function(n) {
    return !!n && Math.random() <= n;
};

async function getPrize() {
    $('.fa-toilet').hide();
    $('.fa-compact-disc').show();
    await new Promise(r => setTimeout(() => r(), 3000));
    $('.fa-compact-disc').hide();
    if (probability(winProbability)) {
        console.log('YAY');
        $('.fa-car-side').show();
        $('#toilet').addClass('btn-winner');
        $( "#message" ).html( "<b>Yay!</b> You got a car!!!" );
    } else {
        console.log('Boo');
        $('#toilet').addClass('btn-loser');
        $( "#message" ).html( "You went on the potty! Good Job!" );
    }
    $('#toilet').off('click');
}
// green and purple